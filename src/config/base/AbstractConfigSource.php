<?php

namespace Idolov\ForExample\config\base;

use Idolov\ForExample\config\interfaces\ConfigSourceInterface;

abstract class AbstractConfigSource implements ConfigSourceInterface
{
    protected $configFilePath;

    public function __construct(string $configFilePath)
    {
        if (!file_exists($configFilePath)) {
            throw new \Exception("Файл $configFilePath не найден.");
        }

        $this->configFilePath = $configFilePath;
    }

    abstract public function getConfigData(): array;
}
