<?php

namespace Idolov\ForExample\config\interfaces;

interface ConfigSourceInterface
{
    public function getConfigData(): array;
}
