<?php

namespace Idolov\ForExample\config\interfaces;

interface SourceFactoryInterface
{
    public function getConfigSource($configPath): ConfigSourceInterface;
}
