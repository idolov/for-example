<?php

namespace Idolov\ForExample\config;

use Idolov\ForExample\config\interfaces\ConfigSourceInterface;
use Idolov\ForExample\config\interfaces\SourceFactoryInterface;

class ConfigSourceFactory implements SourceFactoryInterface
{
    public function getConfigSource($configPath): ConfigSourceInterface
    {
        $fileExtension = pathinfo($configPath, PATHINFO_EXTENSION);

        if ($fileExtension === 'php') {
            return new PhpConfigSource($configPath);
        }

        throw new \Exception("Конфигурационные файлы с расширением $fileExtension не поддерживаются");
    }
}
