<?php

namespace Idolov\ForExample\config;

use Idolov\ForExample\config\interfaces\SourceFactoryInterface;

class ConfigLoader
{
    /** @var SourceFactoryInterface */
    private $sourceFactory;

    private $configs = [];

    function __construct(SourceFactoryInterface $sourceFactory)
    {
        $this->sourceFactory = $sourceFactory;
    }

    public function getRequiredConfig(string $configName)
    {
        if (!isset($this->configs[$configName])) {
            throw new \LogicException("Config \"$configName\" not set");
        }

        return $this->configs[$configName];
    }

    public function getConfig(string $configName, $default = null)
    {
        return $this->configs[$configName] ??  $default;
    }
    
    public function loadConfig($configPath)
    {
        $configData = $this->getConfigData($configPath);
        $this->configs = array_replace($this->configs, $configData);
    }

    public function getConfigData($configFilePath): array
    {
        return $this->sourceFactory->getConfigSource($configFilePath)->getConfigData();
    }
}
