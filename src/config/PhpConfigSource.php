<?php

namespace Idolov\ForExample\config;

use Idolov\ForExample\config\base\AbstractConfigSource;

class PhpConfigSource extends AbstractConfigSource
{
    public function __construct(string $configFilePath)
    {
        if (pathinfo($configFilePath, PATHINFO_EXTENSION) !== 'php') {
            throw new \Exception('Конфигурационный файл должен иметь расширение ".php"');
        }

        parent::__construct($configFilePath);
    }

    public function getConfigData(): array
    {
        $config = require($this->configFilePath);

        if (!is_array($config) || !$config) {
            throw new \Exception("Ошибка загрузки файла конфигурации $this->configFilePath.
                Файл должен содержать массив.");
        }

        return $config;
    }
}
