<?php

namespace Idolov\ForExample;

use Idolov\ForExample\config\ConfigLoader;
use Idolov\ForExample\main\Application;
use Idolov\ForExample\main\ControllerProvider;
use Idolov\ForExample\main\MainController;
use Idolov\ForExample\main\Request;
use Idolov\ForExample\routing\CallableRoutingService;
use Idolov\ForExample\templating\RendererBridge;
use Idolov\ForExample\templating\Templating;
use Idolov\ForExample\uploading\Uploading;
use League\Container\ServiceProvider\AbstractServiceProvider;

class DependencyInjection extends AbstractServiceProvider
{
    protected $provides = [
        ConfigLoader::class,
        Application::class,
        CallableRoutingService::class,
        ControllerProvider::class,
        Uploading::class,
        Templating::class,
        RendererBridge::class,
        RendererBridge::class,
        MainController::class,
        Templating::class,
        ControllerProvider::class,
        Request::class
    ];

    /**
     * @inheritDoc
     */
    public function register()
    {
        /** @var ConfigLoader */
        $configLoader = $this->container->get(ConfigLoader::class);

        $this->leagueContainer->add(Application::class)
            ->addArgument(CallableRoutingService::class)
            ->addArgument(ControllerProvider::class);

        $this->leagueContainer->add(Uploading::class)
            ->addMethodCalls([
                'setUploadsDir' => [$configLoader->getRequiredConfig('uploadsDir')],
                'setUploadsUrl' => [$configLoader->getRequiredConfig('uploadsUrl')],
            ]);

        $this->leagueContainer->add(RendererBridge::class);

        $this->leagueContainer->add(Templating::class)
            ->addArgument(RendererBridge::class)
            ->addMethodCalls([
                'setTemplatesdir' => [$configLoader->getRequiredConfig('templatesDir')],
            ]);

        $this->leagueContainer->add(MainController::class)
            ->addArgument(Templating::class);

        $this->leagueContainer->add(ControllerProvider::class);
        $this->leagueContainer->add(Request::class);

        $this->registerRouting($configLoader);
    }

    private function registerRouting(ConfigLoader $configLoader)
    {
        $defaultRoute = $configLoader->getConfig('defaultRoute');
        $router = $this->leagueContainer->add(CallableRoutingService::class)
            ->addMethodCall('setRoutes', [$configLoader->getRequiredConfig('routes')]);

        if ($defaultRoute) {
            $router->addMethodCall('setDefaultRoute', [$defaultRoute]);
        }
    }
}
