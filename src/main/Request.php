<?php

namespace Idolov\ForExample\main;

use Idolov\ForExample\main\interfaces\RequestInterface;
use Idolov\ForExample\routing\exceptions\NeedHomePageException;
use Idolov\ForExample\routing\exceptions\NotFoundException;

class Request implements RequestInterface
{
    private $requestUri;

    public function getRequestUri()
    {
        if (!$this->requestUri) {
            $this->prepareRequestUri();
        }
        return $this->requestUri;
    }

    public function paramsGet($paramName = null, $defaultValue = null)
    {
        if ($paramName) {
            return isset($_GET[$paramName]) ? $_GET[$paramName] : $defaultValue;
        }

        return $_GET;
    }

    protected function prepareRequestUri()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $uriComponents = parse_url($_SERVER['REQUEST_URI']);
            $this->requestUri = $uriComponents['path'];

            if ($this->requestUri === '/') {
                throw new NeedHomePageException();
            }
        } else {
            throw new NotFoundException();
        }
    }

    public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    public function isPost()
    {
        return isset($_SERVER['REQUEST_METHOD']) && !strcasecmp($_SERVER['REQUEST_METHOD'], 'POST');
    }

    public function redirect($url, $statusCode = 302)
    {
        header('Location: ' . $url, true, $statusCode);
        exit;
    }
}
