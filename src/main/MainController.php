<?php

namespace Idolov\ForExample\main;

use Idolov\ForExample\templating\Templating;

class MainController
{
    /** @var Templating */
    private $templating;

    public function __construct(Templating $templating)
    {
        $this->templating = $templating;
    }

    public function actionNotFound($message = null)
    {
        header("HTTP/1.0 404 Not Found");
        $this->templating->renderPart('main/defaultMessage.php', compact('message'));
    }

    public function actionForbidden($message = null)
    {
        header('HTTP/1.0 403 Forbidden');
        $this->templating->renderPart('main/defaultMessage.php', compact('message'));
    }
}
