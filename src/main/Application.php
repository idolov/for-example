<?php

namespace Idolov\ForExample\main;

use Idolov\ForExample\main\interfaces\ApplicationInterface;
use Idolov\ForExample\main\interfaces\RequestInterface;
use Idolov\ForExample\routing\exceptions\ForbiddenException;
use Idolov\ForExample\routing\exceptions\NeedHomePageException;
use Idolov\ForExample\routing\exceptions\NotFoundException;
use Idolov\ForExample\routing\interfaces\RoutingServiceInterface;
use Psr\Container\ContainerInterface;

class Application implements ApplicationInterface
{
    /** @var RoutingServiceInterface */
    protected $routingService;

    /** @var ControllerProvider */
    protected $controllerProvider;

    public function __construct(
        RoutingServiceInterface $routingService,
        ControllerProvider $controllerProvider
    ) {
        $this->routingService = $routingService;
        $this->controllerProvider = $controllerProvider;
    }

    public function handleRequest(RequestInterface $request)
    {
        try {
            $this->routingService->followRoute($request->getRequestUri(), $request->paramsGet());
        } catch (NeedHomePageException $e) {
            $this->routingService->followDefaultRoute();
        } catch (ForbiddenException $e) {
            $this->controllerProvider->action(MainController::class, 'actionForbidden', [
                'message' => $e->getMessage()
            ]);
        } catch (NotFoundException $e) {
            $this->controllerProvider->action(MainController::class, 'actionNotFound', [
                'message' => $e->getMessage()
            ]);
        }
    }
}
