<?php

namespace Idolov\ForExample\main\interfaces;

interface RequestInterface
{
    public function getRequestUri();
    public function paramsGet();
}
