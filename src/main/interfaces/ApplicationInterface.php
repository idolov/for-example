<?php
namespace Idolov\ForExample\main\interfaces;

use League\Container\Container;

interface ApplicationInterface
{
    public function handleRequest(RequestInterface $request);
}
