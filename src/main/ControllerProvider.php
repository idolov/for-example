<?php

namespace Idolov\ForExample\main;

use Idolov\ForExample\Kernel;

class ControllerProvider
{
    public function action($className, $actionName, $params = [])
    {
        $reflectionClass = new \ReflectionClass($className);

        try {
            $reflectionMethod = $reflectionClass->getMethod($actionName);
        } catch (\ReflectionException $e) {
            throw new \Exception("Действие $actionName не обнаружено в контроллере $className");
        }

        if (!$reflectionMethod->isPublic() || $reflectionMethod->isStatic()) {
            throw new \Exception('В качеcтве действия контроллера
                может быть использован публичный нестатический метод');
        }

        $actionParams = [];
        if ($reflectionParams = $reflectionMethod->getParameters()) {
            foreach ($reflectionParams as $reflectionParam) {
                $paramName = $reflectionParam->getName();
                if (isset($params[$paramName])) {
                    $actionParams[] = $params[$paramName];
                }
            }
        }

        return $reflectionMethod->invokeArgs(Kernel::getContainer()->get($className), $actionParams);
    }
}
