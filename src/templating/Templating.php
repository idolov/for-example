<?php

namespace Idolov\ForExample\templating;

use Idolov\ForExample\templating\interfaces\RendereBridgeInterface;

class Templating
{
    private $layout;
    /** @var RendereBridgeInterface */
    private $rendererBridge;
    private $templatesDir;

    public function __construct(RendereBridgeInterface $rendererBridge)
    {
        $this->rendererBridge = $rendererBridge;
    }

    public function setLayout(string $layout)
    {
        $this->layout = $layout;
    }

    public function setTemplatesDir(string $templatesDir)
    {
        $this->templatesDir = $templatesDir;
    }

    public function render(string $templatePath, array $vars = [])
    {
        if (!$this->layout) {
            throw new \Exception('Не указан layout');
        }

        $this->renderPart($this->layout, [
            'content' => $this->renderPartToSring($templatePath, $vars)
        ]);
    }

    public function renderPart(string $templatePath, array $vars = [])
    {
        $templatePath = ($this->templatesDir)
            ? $this->templatesDir . DIRECTORY_SEPARATOR . $templatePath
            : $templatePath;

        $this->renderTemplate($templatePath, $vars);
    }

    public function renderPartToSring(string $templatePath, array $vars = [])
    {
        ob_start();
        ob_implicit_flush(false);
        $this->renderPart($templatePath, $vars);

        return ob_get_clean();
    }

    final private function renderTemplate(string $templatePath, array $vars = [])
    {
        $this->rendererBridge->render($templatePath, $vars);
    }
}
