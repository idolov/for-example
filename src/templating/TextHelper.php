<?php

namespace Idolov\ForExample\templating;

class TextHelper
{
    private static $ten = [
        ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
    ];

    private static $a20 = [
        'десять',
        'одиннадцать',
        'двенадцать',
        'тринадцать',
        'четырнадцать',
        'пятнадцать',
        'шестнадцать',
        'семнадцать',
        'восемнадцать',
        'девятнадцать',
    ];

    private static $tens = [
        2 => 'двадцать',
        'тридцать',
        'сорок',
        'пятьдесят',
        'шестьдесят',
        'семьдесят',
        'восемьдесят',
        'девяносто',
    ];

    private static $hundred = [
        '',
        'сто',
        'двести',
        'триста',
        'четыреста',
        'пятьсот',
        'шестьсот',
        'семьсот',
        'восемьсот',
        'девятьсот',
    ];

    private static $unit = [
        ['тысяча', 'тысячи', 'тысяч', 1],
        ['миллион', 'миллиона', 'миллионов', 0],
        ['миллиард', 'милиарда', 'миллиардов', 0],
    ];

    private static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }

        return $f5;
    }

    public static function priceToWord($num)
    {
        $num = self::cleanNum($num);

        if (intval($num) === 0) {
            return 'ноль';
        }

        // Зеркально отражаем число
        $reversNum = self::reverseNum($num);


        $parts = str_split($reversNum, 3);

        if (sizeof(static::$unit) < (sizeof($parts) - 1)) {
            return 'Неприлично большое число';
        }

        $words = [];
        foreach($parts as $uk => $part) { // by 3 symbols
            if (!intval($part)) {
                continue;
            }

            $uk -= 1;
            $part = static::reverseNum($part);
            $gender = isset(static::$unit[$uk]) ? static::$unit[$uk][3] : 0;

            list($i1, $i2, $i3) = array_map('intval', str_split(sprintf("%03d", $part), 1));

            $magnitude = [];
            // Десятки сотни, тысячи
            $hundred = static::$hundred[$i1]; // 1xx-9xx
            if($hundred) {
                $magnitude[] = $hundred;
            }

            if ($i2 > 1){
                $magnitude[] = static::$tens[$i2] . ' ' . static::$ten[$gender][$i3];  // 20-99
            } else {
                $magnitude[] = $i2>0 ? static::$a20[$i3] : static::$ten[$gender][$i3]; // 10-19 | 1-9
            }

            // Добавляем величину
            if (isset(static::$unit[$uk])) {
                $magnitude[] = static::morph(
                    $part, static::$unit[$uk][0], static::$unit[$uk][1], static::$unit[$uk][2]
                );
            }

            $words[] = join(' ', $magnitude);
        }

        return join(' ', array_reverse($words));
    }

    private static function reverseNum($num)
    {
        return join( array_reverse( str_split((string)$num,1) ) );
    }

    /**
     * Очищает число от символов
     * @param mixed $num
     * @return string
     */
    public static function cleanNum($num)
    {
        return preg_replace("/[^0-9]/", '', (string)$num);
    }
}
