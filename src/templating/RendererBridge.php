<?php

namespace Idolov\ForExample\templating;

use Idolov\ForExample\templating\interfaces\RendereBridgeInterface;

class RendererBridge implements RendereBridgeInterface
{
    /**
     * @throws \Exception
     */
    public function render(string $templatePath, array $vars = [])
    {
        $renderer =  new Renderer($templatePath, $vars);
        $renderer->render();
    }
}
