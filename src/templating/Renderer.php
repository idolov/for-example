<?php

namespace Idolov\ForExample\templating;

class Renderer
{
    private $vars = [];
    private $templatePath;

    public function __construct(string $templatePath, array $vars = null)
    {
        if (!file_exists($templatePath)) {
            throw new \Exception("Шаблон $templatePath не найден");
        }
        $this->templatePath = $templatePath;
        $this->vars = $vars;
    }

    public function render()
    {
        extract($this->vars, EXTR_PREFIX_SAME, 'var');
        require($this->templatePath);
    }
}
