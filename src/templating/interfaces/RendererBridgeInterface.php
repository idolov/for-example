<?php

namespace Idolov\ForExample\templating\interfaces;

interface RendererBridgeInterface
{
    public function connectRenderer();
}
