<?php

namespace Idolov\ForExample\templating\interfaces;

interface RendereBridgeInterface
{
    public function render(string $templatePath, array $vars = []);
}
