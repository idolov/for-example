<?php

namespace Idolov\ForExample\form;

trait ValidationTrait
{
    private $errors = [];

    public function addError($field, $message)
    {
        if (!is_array($this->errors[$field])) {
            $this->errors[$field] = [];
        }

        $this->errors[$field][] = $message;
    }

    public function hasErrors($field = null)
    {
        return $field
            ? !empty($this->errors[$field])
            : !empty($this->errors);
    }

    public function getErrors($field = null)
    {
        return $field
            ? $this->errors[$field]
            : $this->errors;
    }

    public function getErrorsList($field = null)
    {
        $errors = $this->getErrors($field);
        $errorList = [];
        array_walk_recursive($errors, function ($item) use(&$errorList) {
            $errorList[] = $item;
        });

        return implode('</br>', $errorList);
    }

    public function getValidClass($field)
    {
        return $this->hasErrors($field) ? 'is-invalid' : '';
    }
}
