<?php

namespace Idolov\ForExample\form;

class Validation
{
    /** @var ValidableInterface */
    private $validable;

    public function __construct(ValidableInterface $validable)
    {
        $this->validable = $validable;
    }

    private function getValue($fieldName)
    {
        return trim($this->validable->$fieldName);
    }
    
    public function required($fieldName)
    {
        $value = $this->getValue($fieldName);

        if (!$value) {
            $this->validable->addError($fieldName, "Поле $fieldName обязательно для заполнения");
        }

        return $this;
    }

    public function alnum($fieldName)
    {
        $value = $this->getValue($fieldName);

        if (!ctype_alnum($value)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно содержать только буквы и циффры");
        }

        return $this;
    }

    public function alphaAndDigit($fieldName)
    {
        $value = $this->getValue($fieldName);

        if (ctype_alpha($value) || ctype_digit($value)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно содержать и циффры и буквы");
        }

        return $this;
    }

    public function email($fieldName)
    {
        $value = $this->getValue($fieldName);

        if ($value && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно содержать адрес электронной почты");
        }

        return $this;
    }

    public function equals($fieldFirst, $fieldSecond)
    {
        $valueFirst = $this->getValue($fieldFirst);
        $valueSecond = $this->getValue($fieldSecond);

        if ($valueFirst !== $valueSecond) {
            $this->validable->addError($fieldSecond, "Поля $fieldFirst и $fieldSecond должны быть равны");
        }

        return $this;
    }

    public function integer($fieldName)
    {
        $value = $this->getValue($fieldName);

        if (!filter_var($value, FILTER_VALIDATE_INT)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно быть целым числом");
        }

        return $this;
    }

    public function captcha($fieldName)
    {
        $value = $this->getValue($fieldName);

        if ($value !== $_SESSION['captcha'])
        {
            $this->validable->addError($fieldName, "Не верное значение поля $fieldName");
        }

        return $this;
    }

    public function lenght($fieldName, $min = null, $max = null)
    {
        if (!$min && !$max) {
            throw new \Exception('Укажите значения $min или $max');
        }

        $value = (string) $this->getValue($fieldName);
        $lenght = strlen($value);

        if ($max && ($lenght > $max)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно содержать не более $max символов");
        }

        if ($min && ($lenght < $min)) {
            $this->validable->addError($fieldName, "Поле $fieldName должно содержать не менее $min символов");
        }

        return $this;
    }
}
