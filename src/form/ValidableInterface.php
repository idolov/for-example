<?php


namespace Idolov\ForExample\form;

interface ValidableInterface
{
    public function validate();
    public function addError($field, $message);
    public function hasErrors($field = null);
}
