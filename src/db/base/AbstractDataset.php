<?php

namespace Idolov\ForExample\db\base;

/**
 * Абстрактный класс базовой модели.
 * Хранит в себе набор заранее определенных полей (FieldNames).
 */
abstract class AbstractDataset
{
    /** @var array Данные модели */
    private $data = [];

    /**
     * Получает список полей данного объекта
     * @return array
     */
    abstract public function getFieldNames();

    /**
     * При создании объекта, поля заполняются только теми данными,
     * из общей массы, которые возвращает метод getFieldNames данного объекта.
     * @param array $data Массив с данными
     */
    public function __construct(array $data = [])
    {
        if (!$data) {
            return;
        }

        $attributesNames = $this->getFieldNames();
        foreach ($attributesNames as $attributeName) {
            $this->data[$attributeName] = isset($data[$attributeName]) ? $data[$attributeName] : null;
        }
    }

    public function setFields(array $data = [])
    {
        if (!$data) {
            return;
        }

        $attributesNames = $this->getFieldNames();
        foreach ($data as $key => $value) {
            if (in_array($key, $attributesNames)) {
                $this->data[$key] = $value;
            }
        }
    }
    
    /**
     * Проверяет, существует ли поле с указанным именем.
     * @param string $name Имя атрибута
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Устанавливает поле в заданное занчение
     * @param string $name Имя поля
     * @param mixed $value Значение
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->data)) {
            $this->data[$name] = $value;
        }
    }

    /**
     * Получает значение поля
     * @param string $name Имя поля
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->getField($name);
    }

    /**
     * Получает значения всех полей. Если поле не было заполнено, оно будет указано в массиве и будет равно null
     * @return array
     */
    public function getData()
    {
        $attributes = [];
        $attributesNames = $this->getFieldNames();
        foreach ($attributesNames as $attributeName) {
            $attributes[$attributeName] = $this->getField($attributeName);
        }

        return $attributes;
    }

    /**
     * Конвертирует модель в массив
     * @param callable|null $actionBefore Действия выполняемые над объектом перед ковертацией в массив
     * @return array
     */
    public function toArray(callable $actionBefore = null)
    {
        if (is_callable($actionBefore)) {
            $actionBefore($this);
        }

        return $this->getData();
    }

    /**
     * Получает значение атрибута, если оно не является пустым
     * @param $name
     * @return mixed|null
     */
    protected function getField($name)
    {
        $attribute = !empty($this->data[$name]) ? $this->data[$name] : null;

        if (($attribute === null) && in_array($name, $this->getFieldNames())) {
            try {
                $getMethod = new \ReflectionMethod(static::class, 'get' . ucfirst($name));
                if ($getMethod->isPublic()
                    && !$getMethod->isStatic()
                ) {
                    $attribute = $getMethod->invoke($this);
                }
            } catch (\ReflectionException $e) {
                return null;
            }
        }

        return $attribute;
    }
}
