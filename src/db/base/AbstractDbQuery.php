<?php

namespace Idolov\ForExample\db\base;

use Idolov\ForExample\db\interfaces\DbConnectionInerface;
use PDOStatement;

abstract class AbstractDbQuery
{
    protected $pk;
    protected $tabeName;

    /** @var PDOStatement */
    private $stmt;

    private function __construct()
    {
        $this->init();
    }

    abstract protected function getDatasetClass();

    /**
     * @return DbConnectionInerface
     */
    abstract public function getConnection();

    protected function init()
    {
        $connection = $this->getConnection();
        if (!($connection instanceof DbConnectionInerface)) {
            throw new \Exception('Соединение с базой должно реализовать интерфейс ' . DbConnectionInerface::class);
        }

        $datasetClass = $this->getDatasetClass();
        if (!is_subclass_of($datasetClass, AbstractDataset::class)) {
            throw new \Exception('Класс объекта данных должен быть унаследован от '
                . AbstractDataset::class);
        }
    }

    /**
     * @return AbstractDbQuery
     */
    public static function query()
    {
        return new static();
    }

    public static function save(AbstractDataset $object)
    {
        $query = static::query();
        $pk = $query->pk;

        if ($object->$pk) {
            return $query->update($object);
        }
        return $query->insert($object);
    }

    private function insert(AbstractDataset $object)
    {
        $data = $object->toArray();
        $pk = $this->pk;
        $params = array_map(function ($item) {
            return ':' . $item;
        }, array_keys($data));

        $result = $this->exec(sprintf(
            "insert into {$this->tabeName} (%s) values (%s)",
            implode(',', array_keys($data)),
            implode(',', $params)
        ), $data);

        if ($result) {
            $object->$pk = $this->getConnection()->getPdo()->lastInsertId();
            return true;
        }

        return false;
    }

    private function update(AbstractDataset $object)
    {
        $data = $object->toArray();
        $pk = $this->pk;
        unset($data[$this->pk]);

        $params = array_map(function ($item) use($pk) {
            return "$item = :$item";
        }, array_keys($data));

        $sql = sprintf(
            "update {$this->tabeName} set %s where {$pk} = :{$pk}",
            implode(', ', $params)
        );

        return $this->exec($sql, array_merge($data, [$pk => $object->$pk]));
    }

    /**
     * @param $sql
     * @param $params
     * @return bool
     */
    protected function exec($sql, $params)
    {
        $this->stmt = static::getConnection()->getPdo()->prepare($sql);
        return $this->stmt->execute($params);
    }

    public function hidrateItem(array $dataItem)
    {
        $datasetClass = $this->getDatasetClass();
        return new $datasetClass($dataItem);
    }

    public function hidrateArray(array $dataItem)
    {
        $datasetClass = $this->getDatasetClass();
        $dataset = new $datasetClass();

        foreach ($dataItem as $fieldName => $value) {
            try {
                $setterMethodName = 'set' . ucfirst($fieldName);
                $setterMethod = new \ReflectionMethod($datasetClass, $setterMethodName);
                if ($setterMethod->isPublic()
                    && !$setterMethod->isStatic()
                ) {
                    $setterMethod->invoke($dataset, $value);
                }
            } catch (\ReflectionException $e) {
                throw new \Exception(sprintf('Метод %s не найден в классе %s', $setterMethodName, $datasetClass));
            }
        }

        return $dataset;
    }

    /**
     * @return PDOStatement
     */
    public function getStmt()
    {
        return $this->stmt;
    }
}
