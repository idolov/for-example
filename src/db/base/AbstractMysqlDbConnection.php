<?php

namespace Idolov\ForExample\db\base;

use Idolov\ForExample\db\interfaces\DbConnectionInerface;
use PDO;

abstract class AbstractMysqlDbConnection implements DbConnectionInerface
{
    /** @var static */
    private static $instance;

    /** @var PDO */
    private $pdo;

    private $host;

    private $dbname;

    private $user;

    private $passwd;

    private $charset = 'utf8';

    private $options;

    private function __construct()
    {
        $this->configure();
        if (!$this->pdo) {
            $this->pdo = new PDO(
                $this->getDsn(),
                $this->user,
                $this->passwd,
                $this->options ?: self::$defaultOptions
            );
        }
    }

    private static $defaultOptions = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    abstract protected function configure();

    public function getDsn()
    {
        return "mysql:host={$this->host};dbname={$this->dbname};charset={$this->charset}";
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function setPasswd($passwd)
    {
        $this->passwd = $passwd;
    }

    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    public static function getConnection()
    {
        if (!self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function query($sql, $params = null)
    {
        $stmt = self::getConnection()->pdo->prepare($sql);
        $stmt->execute($params);

        return $stmt;
    }
}
