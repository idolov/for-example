<?php

namespace Idolov\ForExample\db\interfaces;

use PDO;

interface DbConnectionInerface
{
    public static function getConnection();

    public function query($sql, $params = null);
    /**
     * @return PDO
     */
    public function getPdo();
}
