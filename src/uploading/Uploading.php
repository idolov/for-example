<?php

namespace Idolov\ForExample\uploading;

class Uploading
{
    private static $uploadsErrors = [
        UPLOAD_ERR_INI_SIZE => 'Размер принятого файла превысил максимально допустимый размер',
        UPLOAD_ERR_FORM_SIZE => 'Размер принятого файла превысил максимально допустимый размер',
        UPLOAD_ERR_PARTIAL => 'Загружаемый файл был получен только частично',
        UPLOAD_ERR_NO_FILE => 'Файл не был загружен',
        UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка',
        UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск',
        UPLOAD_ERR_EXTENSION => 'Загрузка файла была остановлена'
    ];

    private $uploadsDir;
    private $uploadsUrl;

    public function setUploadsDir($uploadsDir)
    {
        $this->uploadsDir = $uploadsDir;
    }

    public function setUploadsUrl($uploadsUrl)
    {
        $this->uploadsUrl = $uploadsUrl;
    }

    public function getUploadPath($relativePath)
    {
        return $this->uploadsDir . DIRECTORY_SEPARATOR . $relativePath;
    }

    public function getUploadUrl($relativeUrl)
    {
        $relativeUrl = trim($relativeUrl, DIRECTORY_SEPARATOR);
        return DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR,
            [$this->uploadsUrl, $relativeUrl]);
    }

    public function uploadFile($source, $dest)
    {
        $dir = pathinfo($dest, PATHINFO_DIRNAME);

        if (!$this->uploadsDir) {
            throw new UploadingFailedException('Не настроена базовая директория для загрузок');
        }

        if (!is_dir($this->uploadsDir) && !@mkdir($this->uploadsDir, 0777, true)) {
            throw new UploadingFailedException('Не создана директория ' . pathinfo($this->uploadsDir, PATHINFO_BASENAME) . ', или не доступна для записи');
        }

        if (!is_dir($dir) && !@mkdir($dir, 0777, true)) {
            throw new UploadingFailedException('Не удалось создать директорию для загрузки файла');
        }

        if (!@move_uploaded_file($source, $dest)) {
            throw new UploadingFailedException('Не удалось переместить файл в директорию ' . $this->uploadsDir);
        }

        return true;
    }

    public function removeFile($filePath)
    {
        return @unlink($filePath);
    }

    public function getErrorText($errNum)
    {
        return isset(self::$uploadsErrors[$errNum]) ? self::$uploadsErrors[$errNum] : 'Неизвестная ошибка';
    }
}
