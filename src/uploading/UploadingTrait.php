<?php

namespace Idolov\ForExample\uploading;

trait UploadingTrait
{
    public function getUploadPath(UploadableInterface $uploadable, $fileName)
    {
        return Uploading::getUploadPath($this->checkUploadDir($uploadable))
            . DIRECTORY_SEPARATOR . $fileName;
    }

    public function getUploadUrl(UploadableInterface $uploadable, $fileName)
    {
        $uploadDir = $this->preparePath($uploadable->getUploadsDir());
        $fileName = $this->preparePath($fileName);
        $path = $this->getUploadPath($uploadable, $fileName);

        return is_file($path)
            ? Uploading::getUploadUrl($uploadDir . DIRECTORY_SEPARATOR . $fileName)
            : null;
    }

    private function checkUploadDir(UploadableInterface $uploadable)
    {
        $relativePath = $uploadable->getUploadsDir();
        if (!$relativePath) {
            throw new \Exception('Не удалось получить директорию для загрузок');
        }

        return $relativePath;
    }

    private function preparePath($path)
    {
        return trim($path, DIRECTORY_SEPARATOR);
    }
}
