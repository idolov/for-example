<?php

namespace Idolov\ForExample\uploading;

interface UploadableInterface
{
    public function getUploadsDir();
}
