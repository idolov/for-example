<?php

namespace Idolov\ForExample\routing\interfaces;

interface RoutingServiceInterface
{
    public function getRoute(string $url);

    public function followRoute(string $url, array $params = []);

    public function followDefaultRoute(array $params = []);
}
