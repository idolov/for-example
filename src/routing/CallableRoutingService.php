<?php

namespace Idolov\ForExample\routing;

use Idolov\ForExample\routing\exceptions\NotFoundException;
use Idolov\ForExample\routing\interfaces\RoutingServiceInterface;

class CallableRoutingService implements RoutingServiceInterface
{
    private $routes = [];

    private $defaultRoute;

    public function setRoutes(array $routes)
    {
        foreach ($routes as $url => $route) {
            $this->routes[$url] = $this->prepareRoute($route);
        }
    }

    public function setDefaultRoute(string $defaultRoute)
    {
        $this->defaultRoute = $defaultRoute;
    }

    public function getRoute($url)
    {
        return $this->routes[$url] ?? null;
    }

    public function followRoute(string $url, array $params = [])
    {
        $route = $this->getRoute($url);

        if (!$route) {
            throw new NotFoundException('Страница не найдена');
        }

        return call_user_func_array($route, $params);
    }

    public function followDefaultRoute(array $params = [])
    {
        if (!$this->defaultRoute) {
            throw new \RuntimeException('Default route not set');
        }

        return $this->followRoute($this->defaultRoute, $params);
    }

    private function prepareRoute($route)
    {
        if (!is_callable($route)) {
            throw new \Exception('Необходима callback функция');
        }

        return $route;
    }
}
