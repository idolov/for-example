<?php

namespace Idolov\ForExample\routing\exceptions;


class NeedHomePageException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'Перенаправление на домашнюю страницу не было обработано';
        parent::__construct($message, $code, $previous);
    }
}
