<?php

namespace Idolov\ForExample\routing\exceptions;

use Throwable;

class NotFoundException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'Страница не найдена';
        parent::__construct($message, $code, $previous);
    }

}
