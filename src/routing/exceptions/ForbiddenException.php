<?php

namespace Idolov\ForExample\routing\exceptions;

class ForbiddenException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = $message ?: 'Доступ запрещен';
        parent::__construct($message, $code, $previous);
    }
}
