<?php

namespace Idolov\ForExample;

use Idolov\ForExample\config\ConfigLoader;
use Idolov\ForExample\config\ConfigSourceFactory;
use Idolov\ForExample\main\interfaces\ApplicationInterface;
use League\Container\Container;
use League\Container\ReflectionContainer;
use League\Container\ServiceProvider\ServiceProviderInterface;
use Psr\Container\ContainerInterface;

class Kernel
{
    /** @var Container */
    private static $container;

    /** @var \Closure */
    private $onInitFunction;

    public static function getContainer(): ContainerInterface
    {
        return self::$container;
    }

    public function __construct()
    {
        self::$container = new Container();
    }

    public function addDependencies(ServiceProviderInterface $serviceProvider): self
    {
        self::$container->addServiceProvider($serviceProvider);

        return $this;
    }

    public function createApp($className): ApplicationInterface
    {
        self::$container->addServiceProvider(DependencyInjection::class);

        return self::$container->get($className);
    }

    public function loadConfig(string $configPath): self
    {
        /** @var ConfigLoader $configLoader */
        $configLoader = self::$container->get(ConfigLoader::class);
        $configLoader->loadConfig($configPath);

        return $this;
    }

    public function onInit(\Closure $onInitFunction): self
    {
        $this->onInitFunction = $onInitFunction;

        return $this;
    }

    public function init(): self
    {
        $this->onInitFunction->call($this, self::$container);

        self::$container->delegate(
            new ReflectionContainer()
        );

        self::$container->add(ConfigSourceFactory::class, null, true);
        self::$container->add(ConfigLoader::class, null, true)
            ->addArgument(ConfigSourceFactory::class);

        return $this;
    }
}
