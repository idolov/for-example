<?php

namespace Idolov\ForExample\sequrity;

use Idolov\ForExample\main\traits\SingletonTrait;

class Captcha
{
    use SingletonTrait;

    private static $simbols = array('a','A','b','B','c','C','d','D','e','E','f','F','g','G','h','H','i','I','j',
            'J','k','K','l','L','m','M','n','N','o','O','p','P','q','Q','r','R','s','S','t','T','u','U',
            'v','V','w','W','z','Z','Y','y','x','X','1','2','3','4','5','6','7','8','9','0');


    public function initCode()
    {
        return $_SESSION['captcha'] = $this->genCode();
    }

    public function genCode()
    {
        $lenghtCode = mt_rand(4, 8);
        $codeChars = '';
        for ($i = 1; $i <= $lenghtCode; $i++) {
            $codeChars .= static::$simbols[(int)mt_rand(0,count(static::$simbols))];
        }
        return $codeChars;
    }

    public static function getFontPath()
    {
        return implode(DIRECTORY_SEPARATOR, [
            __DIR__, 'ARIAL.TTF'
        ]);
    }
}
