<?php

namespace Idolov\ForExample\sequrity;

use Specstroy\db\models\User;
use Specstroy\db\queries\UsersQuery;

class Auth
{
    public static function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public static function verifyPassword($password, $hash)
    {
        return password_verify($password, $hash);
    }

    public static function grantAccess(User $user)
    {
        $_SESSION['userId'] = $user->id;
        $_SESSION['login'] = $user->username;
    }

    public static function getUser()
    {
        if (isset($_SESSION['login']) && isset($_SESSION['userId'])) {
            
            $user = UsersQuery::getByUsername($_SESSION['login']);

            return $user && ($user->id == $_SESSION['userId']) ? $user : null;
        }

        return null;
    }

    public static function signOut()
    {
        unset($_SESSION['userId'], $_SESSION['login']);
    }
}


