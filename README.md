 

## Readme

#### Example web/index.php 

```php
<?php

use Idolov\ForExample\Kernel;
use Idolov\ForExample\main\Application;
use Idolov\ForExample\main\Request;
use Idolov\Example\DependencyInjection;
use Psr\Container\ContainerInterface;

session_start();
require __DIR__ . '/../../vendor/autoload.php';

$kernel = (new Kernel())
    ->onInit(function (ContainerInterface $container) use ($someModule) {
        //$container->delegate($someModule->getContainer());
    })
    ->init()
    ->addDependencies(new DependencyInjection())
    ->loadConfig(__DIR__ . '/../config/app.php')
    ->loadConfig(__DIR__ . '/../config/db.php');
    ->loadConfig(__DIR__ . '/../config/routing.php');

$app = $kernel->createApp(Application::class);
$app->handleRequest(new Request());
```
